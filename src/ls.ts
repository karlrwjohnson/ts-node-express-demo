import fs from 'fs';
import path from 'path';

(async () => {
    const dir = path.resolve(__dirname, '..');
    for (const entry of await fs.promises.readdir(dir, { encoding: 'utf-8', withFileTypes: true })) {
        const type =
            entry.isFile() ? 'file' :
            entry.isDirectory() ? 'directory' :
            entry.isBlockDevice() ? 'block device' :
            entry.isCharacterDevice() ? 'character device' :
            'unknown';
        console.log(`${entry.name} - ${type}`);
    }
})();

