import { Client } from 'pg';
import { injectableProxy } from './util/injectableProxy';

let database: Client | null = null;

export function setDatabase(newDatabase: Client) {
    database = newDatabase;
}

export const db = injectableProxy<Client>(() => {
    if (!database) {
        throw new Error('Database is uninitialized');
    }
    return database;
});
