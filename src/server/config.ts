import { ClientConfig } from 'pg';

export const PORT = 3000;

export const dbConfig: ClientConfig = {
    // Yes it's a real password.
    // No, it's not a security issue. It's my local, personal machine.
    // Yes, of course I'd load these credentials differently if this was a real webservice.
    user: 'karl',
    database: 'postgres',
    password: '213616',
    port: 5432,
    host: 'localhost',
};
