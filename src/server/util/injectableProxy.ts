/**
 * Allows you to give consumers references to a thing before it's even been created.
 * @param provider
 */
export function injectableProxy<T extends object>(provider: () => T): T {
    return new Proxy<T>({} as any, {
        getPrototypeOf: (_: unknown): object | null =>
            Object.getPrototypeOf(provider()),
        setPrototypeOf: (_: unknown, v: any): boolean =>
            Object.setPrototypeOf(provider(), v),
        isExtensible: (_: unknown): boolean =>
            Object.isExtensible(provider()),
        preventExtensions: (_: unknown): boolean => {
            const provided = provider();
            if (Object.isExtensible(provided)) {
                Object.preventExtensions(provided);
                return false;
            } else {
                return true;
            }
        },
        getOwnPropertyDescriptor: (_: unknown, p: PropertyKey): PropertyDescriptor | undefined =>
            Object.getOwnPropertyDescriptor(provider(), p),
        has: (_: unknown, p: PropertyKey): boolean =>
            p in provider(),
        get: (_: unknown, p: PropertyKey, receiver: any): any =>
            (provider() as any)[p],
        set: (_: unknown, p: PropertyKey, value: any, receiver: any): boolean => {
            (provider() as any)[p] = value;
            return true;
        },
        deleteProperty: (_: unknown, p: PropertyKey): boolean =>
            delete (provider() as any)[p],
        defineProperty: (_: unknown, p: PropertyKey, attributes: PropertyDescriptor): boolean =>
            Object.defineProperty(provider(), p, attributes),
        ownKeys: (_: unknown): PropertyKey[] =>
            [...Object.getOwnPropertyNames(provider()), ...Object.getOwnPropertySymbols(provider())],
        apply: (_: unknown, thisArg: any, argArray?: any): any =>
            (provider() as any).apply(thisArg, argArray),
        construct: (_: unknown, argArray: any, newTarget?: any): object =>
            new (provider() as any)(...argArray),
    });
}
