export function isUUID(str: string): boolean {
    return Boolean((/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/i).exec(str));
}