import {
    NextFunction,
} from 'express';
import {
    Params,
    ParamsDictionary,
    Request,
    RequestHandler,
    Response,
} from 'express-serve-static-core';

export function desync<P extends Params = ParamsDictionary>(
    routeHandler: (req: Request<P>, res: Response) => any | Promise<any>
): RequestHandler<P> {
    return async function callRouteHandler (req: Request<P>, res: Response, next: NextFunction) {
        try {
            await routeHandler(req, res);
        } catch(e) {
            next(e);
        }
    }
}
