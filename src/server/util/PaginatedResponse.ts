export interface PaginatedResponse<T> {
    success: boolean;
    data: T[],
    page: number;
    pageCount: number;
    pageResultCount: number;
    total: number;
}
