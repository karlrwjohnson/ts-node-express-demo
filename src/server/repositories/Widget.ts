import { Dictionary } from 'express-serve-static-core';
import { db } from '../database';

export interface Widget {
    id: string;
    name: string;
    price: number;
    data: Dictionary<any>;
}

export const getAllWidgets = async (): Promise<Widget[]> =>
    (await db.query(`select id, name, price, data from widgets`)).rows;

export const createNewWidget = async (name: string, price: number, data: Dictionary<any>): Promise<Widget> =>
    (await db.query(`
        insert into widgets(name, price, data)
        values ($1, $2, $3)
        returning id, name, price, data
    `, [name, price, data])).rows[0];

export const getWidgetById = async (id: string): Promise<Widget | undefined> =>
    (await db.query(`
        select id, name, price, data
        from widgets
        where id = $1
    `, [id])).rows[0];

export const deleteWidgetById = async (id: string): Promise<number> =>
    (await db.query(`
        delete from widgets
        where id = $1
    `, [id])).rowCount;

export const getWidgetByName = async (name: string): Promise<Widget | undefined> =>
    (await db.query(`
        select id, name, price, data
        from widgets
        where name = $1
    `, [name])).rows[0];

export const deleteWidgetByName = async (name: string): Promise<number> =>
    (await db.query(`
        delete from widgets
        where name = $1
    `, [name])).rowCount;
