import { Client } from 'pg';

export class BaseRepository {
    protected db: Client;

    constructor(db: Client) {
        this.db = db;
    }
}


