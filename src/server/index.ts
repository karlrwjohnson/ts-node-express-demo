import express from 'express';
import { Client } from 'pg';
import {
    dbConfig,
    PORT,
} from './config';
import { setDatabase } from './database';
import { handleErrors } from './errors/handleErrors';
import { registerRoutes } from './routeHandlers';

async function main() {
    const app = express();

    const db = new Client(dbConfig);
    await db.connect();
    setDatabase(db);

    app.use(handleErrors);
    app.use(express.json());

    registerRoutes(app);

    app.listen(PORT, () => console.log(`Example app listening on port ${PORT}`));
}

main();
