import { HttpStatus } from '../util/HttpStatus';

export class StatusCodeError extends Error {
    public readonly statusCode: HttpStatus;

    constructor(statusCode: HttpStatus, message: string) {
        super(message);
        this.statusCode = statusCode;
    }
}
