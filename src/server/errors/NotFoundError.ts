import { HttpStatus } from '../util/HttpStatus';
import { quickThrower } from './quickThrower';
import { StatusCodeError } from './StatusCodeError';

interface Class<T> {
    new (...args: any): T;
}

type Type = string | Class<any>;

function formatType(type: Type) {
    return (typeof type === 'string') ? type : type.name;
}

type Criteria = string | { [key: string]: any };

function oxfordCommaSeparate(parts: string[]): string {
    switch (parts.length) {
        case 0: return '';
        case 1: return parts[0];
        case 2: return parts.join(' and ');
    }

    const most = parts.slice(0, parts.length - 2);
    const last = parts.slice(parts.length - 1);
    return most.join(', ') + ', and ' + last;
}

function formatCriteria(criteria: Criteria) {
    if (typeof criteria === 'string') return criteria;

    return oxfordCommaSeparate(Object.entries(criteria).map(([k, v]) => `${k} = ${v}`));
}

export class NotFoundError extends StatusCodeError {
    readonly type: Type;
    readonly criteria: Criteria;

    constructor(type: Type, criteria: Criteria) {
        super(HttpStatus.NOT_FOUND, `No ${formatType(type)} exists matching ${formatCriteria(criteria)}`);
        this.type = type;
        this.criteria = criteria;
    }
}

export const notFound = quickThrower(NotFoundError);
