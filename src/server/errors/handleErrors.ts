import { NextFunction } from 'express';
import {
    Request,
    Response,
} from 'express-serve-static-core';
import { StatusCodeError } from './StatusCodeError';

export function handleErrors(e: unknown, req: Request, res: Response, next: NextFunction) {
    if (!res.headersSent) {
        if (e instanceof StatusCodeError) {
            res.sendStatus(e.statusCode);
            res.send({ error: e.message, statusCode: e.statusCode, stack: e.stack });
        } else if (e instanceof Error) {
            res.sendStatus(500);
            res.send({ error: e.message, statusCode: 500, stack: e.stack });
        } else {
            res.sendStatus(500);
            res.send({ error: JSON.stringify(e), statusCode: 500 });
        }
    } else {
        return next(e);
    }
}
