export function raise<T extends Error>(error: T): never {
    throw error;
}
