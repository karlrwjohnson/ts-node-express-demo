interface Class<T> { new (...args: any): T; }

/**
 * Constructs and throws an error. The error's constructor is curried.
 */
export function quickThrower<E extends Error>(ErrorClass: Class<E>) {
    return (...args: ConstructorParameters<Class<E>>) => {
        throw new ErrorClass(...args);
    }
}
