import {
    Request,
    Response,
} from 'express-serve-static-core';
import {
    notFound,
    NotFoundError,
} from '../errors/NotFoundError';
import { raise } from '../errors/raise';
import {
    createNewWidget,
    deleteWidgetById,
    deleteWidgetByName,
    getAllWidgets,
    getWidgetById,
    getWidgetByName,
    Widget,
} from '../repositories/Widget';
import { StatusCodeError } from '../errors/StatusCodeError';
import { HttpStatus } from '../util/HttpStatus';
import { isUUID } from '../util/isUUID';
import { PaginatedResponse } from '../util/PaginatedResponse';

type CreateWidgetPayload = Pick<Widget, 'name' | 'price' | 'data'>

export function validateWidget(data: any): CreateWidgetPayload {
    if (typeof data === 'object' &&
        data !== null &&
        ('name' in data && typeof data['name'] === 'string') &&
        ('price' in data && typeof data['price'] === 'number') &&
        ('data' in data && typeof data['data'] === 'object' && data['data'] !== null)
    ) {
        return data;
    }
    throw new StatusCodeError(HttpStatus.BAD_REQUEST, 'Invalid create payload');
}

export async function list_widgets(req: Request, res: Response) {
    const widgets = await getAllWidgets();
    const response: PaginatedResponse<Widget> = {
        success: true,
        data: widgets,
        // Fake pagination
        page: 1,
        pageCount: 1,
        pageResultCount: widgets.length,
        total: widgets.length,
    };
    res.send(response);
}

export async function create_widget(req: Request, res: Response) {
    const requestWidget = validateWidget(req.body);
    const createdWidget = await createNewWidget(requestWidget.name, requestWidget.price, requestWidget.data);
    res.status(HttpStatus.CREATED).json(createdWidget);
}

export async function get_widget(req: Request<{ widgetIdOrName: string }>, res: Response) {
    const { widgetIdOrName } = req.params;
    const widget: Widget = isUUID(widgetIdOrName) ?
        await getWidgetById(widgetIdOrName) || notFound('widget', { id: widgetIdOrName }) :
        await getWidgetByName(widgetIdOrName) || notFound('widget', { name: widgetIdOrName });

    res.json(widget);
}

export async function delete_widget(req: Request<{ widgetIdOrName: string }>, res: Response) {
    const { widgetIdOrName } = req.params;
    isUUID(widgetIdOrName) ?
        await deleteWidgetById(widgetIdOrName) || notFound('widget', { id: widgetIdOrName }) :
        await deleteWidgetByName(widgetIdOrName) || notFound('widget', { name: widgetIdOrName });

    res.status(HttpStatus.NO_CONTENT).send();
}

