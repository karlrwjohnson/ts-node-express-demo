import {
    Request,
    Response,
} from 'express-serve-static-core';

export function get_root(req: Request, res: Response) {
    res.send({ greeting: 'Hello world!' });
}
