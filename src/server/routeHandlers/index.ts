import { Express } from 'express';
import { desync } from '../util/desync';
import { get_root } from './root';
import {
    create_widget,
    delete_widget,
    get_widget,
    list_widgets,
} from './widgets';

export function registerRoutes(app: Express) {
    app.get('/', desync(get_root));
    app.get('/widgets', desync(list_widgets));
    app.post('/widgets', desync(create_widget));
    app.get('/widgets/:widgetIdOrName', desync(get_widget));
    app.delete('/widgets/:widgetIdOrName', desync(delete_widget));
}

