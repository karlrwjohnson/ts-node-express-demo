# ts-node + express demo

This project demonstrates how using the `ts-node` CLI as a drop-in replacement
for `node` enables you to write backend services using Typescript.

With `ts-node`, no transpilation is necessary. I think it internally pipes the
compiled code from Typescript directly into Node. There's a little bit of start-up
delay because Typescript needs to do "its thing", but if you're running in prod
you can probably pass the "--transpile-only" flag  for even quicker startup time.

It's still faster than Java, anyway :)

### Not Typescript-related things

- I'm learning how to use Express as I do this. I've briefly looked at CMX so
    that's my only idea for how to structure a Node webservice.

- I originally started making static global functions for everything, but when I
    realized they'd all have to reference the same global `db` reference, I
    briefly considered going fully Inversion of Control. But then I figured out
    I could "swap out" the `db` object using a Proxy. I don't know if this is a
    common pattern or not. So far, I think I like it better.

- My database queries are string literals. My IDE (IntelliJ) is smart enough to
    recognize SQL queries in string constants and perform syntax highlighting
    within them.

### The Bad

- My biggest gripe with Typescript is that it doesn't exist at runtime. It can
    guard against stupidity when you call your own code by passing the wrong-
    shaped object to a function or don't handle a possible return type, but
    it won't ensure that you wrote proper type checks when you get data passed
    in. The best thing you can do is declare the outside type as "unknown" and
    start writing type check guards for validation until Typescript starts
    letting you access properties on the thing without throwing up its hands.


### The Good

- The joy of Typescript comes when your IDE/editor knows what you're doing and
    starts suggesting methods and parameters based on the types it infers.
    This makes development much faster.

- Typescript is also like 50% of the documentation you need for a project. If you
    see a function with a parameter named `url` and it's a string, you know it's
    a URL string and not some wrapper class.

- Typescript will understand basic "if" statements and "typeof" checks to infer
    the type of a variable for the rest of a block. For example, if something
    can be "Foo | undefined" and you check whether it's "truthy", Typescript
    will tell Intellisense to suggest suggest methods and properties of Foo
    whenever you type "foo<dot>"

- User-defined type guards are useful
    (https://www.typescriptlang.org/docs/handbook/advanced-types.html#user-defined-type-guards)

### Tips

- Never ever ever use `any` if you can help it. Since `any` turns off Typescript's
    type checker, Typescript will silently let you do illegal things.
    IMO the only reason to use it is as a parameter to a type macro.

- Always define the types of your function parameters. Don't allow "implicit any".

- It's good to have a grasp of Generics first, then pick up Algebraic data types
    (i.e. union and intersection types).

- Also start reading the macro definitions in (lib.es5.ts)[https://github.com/microsoft/TypeScript/blob/master/lib/lib.es5.d.ts#L1421].
    Knowing how `Partial<T>` and `Parameters<T>` work helps teach you how to write
    your own macros.
